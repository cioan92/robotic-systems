#ifndef _Line_follow_h
#define _Line_follow_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "pins.h"
#include "definitions.h"
#include "move.h"

const int NUM_CALIBRATIONS = 20;

class LineSensor
{
    public:
        LineSensor(int pin);
        void calibrate();
        int  readRaw();
        float read_calibrated();
		void setThreshHold(float value);
		float getThreshHold();
		bool isBlack();
		bool keepMeIn();
		void printDebug();
    
    private:

        float calibration_offset=0;
        int pin;
		float threshHold = 300;
		bool goInside = false;
		bool rotationFinished = false;
		bool straightMoveFinished = true;
		bool lineSensorDebug = true;
		long elapsed = millis();
};	

#endif
