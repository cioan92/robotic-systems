// 
// 
// 
#include "control.h"

PID leftPose(0.20, 1.5, 0);		//Position controller for left wheel position
PID rightPose(0.25, 1.5, 0);		//Position controller for right wheel position

// Angle PID
PID leftAngle(3, 1.5, 0);			//Angle controller for left wheel 
PID rightAngle(3, 1.5, 0);			//Angle controller for right wheel 

// Speed PID
PID leftSpeed(0.5, 1.5, 0);			//Speed controller for left wheel 
PID rightSpeed(0.5, 1.5, 0);		//Speed controller for right wheel 

// IR follow PID
PID leftIR(4, 1.5, 0.00005);		//IR controller for left wheel 
PID rightIR(4, 1.5, 0.00005);		//IR controller for right wheel 

// Speed-Angle PID 
// (Speed is adjusted based on changes in angle)
PID leftSpeedA(0.5, 0, 0);			//Speed controller for left wheel 
PID rightSpeedA(0.5, 0, 0);			//Speed controller for right wheel 

// Line Following PID
//PID lineLeftSpeed(4, 1.5, 0);
//PID lineRightSpeed(3.323, 1.5, 0);
/* ----------------------------------------------------------------------------------*/
//
bool enable_pos_PID;
bool enable_spd_PID;
bool enable_line_PID;

float outputI, outputII, outputIII, outputIV;

/* double line_orientation()
	Calculates a number that designates the orientation of the robot, when
	placed under a black (reflective) line
	Return Arguments:
	1000: left
	2000: centered
	3000: right
*/
double line_orientation() {

	double lft = 0;// LineLeft.read_calibrated();
	double rgt = 0;// LineRight.read_calibrated();
	double cnt = LineCentre.read_calibrated();

	double total = (lft + rgt + cnt);

	if (total == 0) {
		total = 1;
	}

	double lft_ = lft / total;
	double rgt_ = rgt / total;
	double cnt_ = cnt / total;

	// Likelihood of sensor being on line centre 
	double line_centre_prob = (lft_ * 1000) + (cnt_ * 2000) + (rgt_ * 3000);

	Serial.print("Left::");
	Serial.print(lft);
	Serial.print("Center::");
	Serial.print(cnt);
	Serial.print("Right::");
	Serial.print(rgt);
	Serial.print("Total Prob::");
	Serial.println(line_centre_prob);

	return line_centre_prob;
}

/* -------------------------------------- PID Controls -----------------------------------------------*/
void refresh_pid_speed_enc(bool feedback) {

	outputIII =  LeftSpeedControl.update(target_speed_left, e0_enc_speed);
	outputIV =  RightSpeedControl.update(target_speed_right, e1_enc_speed);

	//outputIII =  LeftSpeedControl.update(target_speed_left, getEnc0Speed());
	//outputIV =  RightSpeedControl.update(target_speed_right, getEnc1Speed());

	if (feedback) {
		left_motor.setPower(outputIII);
		right_motor.setPower(outputIV);
	}
}

void refresh_pid_speed(bool feedback) {

	// Speed PID Control
	if (moveType <= 1) {
		outputIII = left_motor.getSpeed() + leftSpeed.update(target_speed_left, getEnc0Speed());
		outputIV = right_motor.getSpeed() + rightSpeed.update(target_speed_right, getEnc1Speed());
	}
	else {
		outputIII = left_motor.getSpeed() + leftSpeedA.update(target_speed_left, getEnc0Speed());
		outputIV = right_motor.getSpeed() + rightSpeedA.update(target_speed_right, getEnc1Speed());
	}

	if (feedback) {
		left_motor.setSpeed(outputIII);
		right_motor.setSpeed(outputIV);
	}
}

void refresh_pid_position(bool feedback) {

	// Position PID Control
	outputI = leftPose.update(target_left, count_e0);
	outputII = rightPose.update(target_right, count_e1);

	if (feedback) {
		left_motor.setSpeed(outputI);
		right_motor.setSpeed(outputII);
	}
}

void refresh_pid_angle(bool feedback) {

	// Position PID Control
	outputI = leftAngle.update(target_left, count_e0);
	outputII = rightAngle.update(target_right, count_e1);
	//Serial.print("Output:[left]");
	//Serial.print(outputI);
	//Serial.print("Output:[right]");
	//Serial.print(outputII);

	if (feedback) {
		left_motor.setSpeed(outputI);
		right_motor.setSpeed(outputII);
	}
}

void refresh_pid_line(bool feedback) {

	double line_centre_prob = line_orientation();

	// Speed PID Control
	target_speed_left = TARGET_SPEED + ((TARGET_SPEED) * (line_centre_prob - 2000) / 1000);
	target_speed_right = TARGET_SPEED - ((TARGET_SPEED) * (line_centre_prob - 2000) / 1000);

	//outputIII = lineLeftSpeed.update(target_speed_left, getEnc0Speed());
	//outputIV = lineRightSpeed.update(target_speed_right, getEnc1Speed());

	if (feedback) {
	//	left_motor.setSpeed(outputIII);
	//	right_motor.setSpeed(outputIV);
	}
}

void refresh_pid_IR(bool feedback) {

	outputIII = -leftIR.update(20, lft_IR_sensor.get_distance_in_cm()) + leftAngle.update(0, radiansToDegrees(Pose.get_theta_radians()));
	outputIV = -rightIR.update(20, lft_IR_sensor.get_distance_in_cm()) - rightAngle.update(0, radiansToDegrees(Pose.get_theta_radians()));

	if (feedback) {
		left_motor.setSpeed(outputIII);
		right_motor.setSpeed(outputIV);
	}
}

void choose_PID_controller(int choice, bool feedback) {

	switch (choice) {
	case NO_PID:
		break;
	case POSITION_PID:
		refresh_pid_position(feedback);
		break;
	case SPEED_PID:
		refresh_pid_speed(feedback);
		break;
	case LINE_PID:
		//refresh_pid_line(feedback);
		break;
	case ANGLE_PID:
		refresh_pid_angle(feedback);
		break;
	case IR_PID:
		//refresh_pid_IR(feedback);
		break;
	case SPEED_ENC:
		refresh_pid_speed_enc(feedback);
		break;
	case IR_OBSTACLE_PID:
		//refresh_pid_obstacle_ir(feedback);
		break;
	default:
		Serial.println("Error [2] :: PID choice");
		break;
	}
}

void reset_pid(int choice) {

	switch (choice) {
	case NO_PID:
		break;
	case POSITION_PID:
		leftPose.reset();
		rightPose.reset();
		break;
	case SPEED_PID:
		leftSpeed.reset();
		rightSpeed.reset();
		leftSpeedA.reset();
		rightSpeedA.reset();
		break;
	case LINE_PID:
		//lineLeftSpeed.reset();
		//lineRightSpeed.reset();
		break;
	case ANGLE_PID:
		leftAngle.reset();
		rightAngle.reset();
		break;
	case IR_PID:
		leftIR.reset();
		rightIR.reset();
		break;
	case SPEED_ENC:
		LeftSpeedControl.reset();
		RightSpeedControl.reset();
		break;
	default:
		Serial.println("Error [2] :: PID choice");
		break;
	}
}

void reset_controls() {
	outputI = 0;
	outputII = 0;
	outputIII = 0;
	outputIV = 0;

	leftAngle.reset();
	rightAngle.reset();
	leftSpeed.reset();
	rightSpeed.reset();
	leftSpeedA.reset();
	rightSpeedA.reset();
	//lineLeftSpeed.reset();
	//lineRightSpeed.reset();
	LeftSpeedControl.reset();
	RightSpeedControl.reset();
	leftIR.reset();
	rightIR.reset();
}

void initialize_controls() {
	leftPose.setMax(255);			// Position		
	rightPose.setMax(255);
	leftSpeed.setMax(50);			// Speed 
	rightSpeed.setMax(50);
	leftSpeedA.setMax(30);			// Speed (for rotation) (bugged - do not use)
	rightSpeedA.setMax(30);
	leftAngle.setMax(25);			// Position (for rotation)
	rightAngle.setMax(28);
	//lineLeftSpeed.setMax(50);		// Speed (for line following)
	//lineRightSpeed.setMax(50);
}