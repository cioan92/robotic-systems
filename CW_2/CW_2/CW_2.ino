
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Library Includes.                                                             *
 * Be sure to check each of these to see what variables/functions are made        *
 * global and accessible.                                                        *
 *                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "definitions.h"			// Basic definitions (flags, switch cases)
#include "pid.h"					// Romi Pins
#include "motors.h"
#include "encoder.h"
#include "pins.h"
#include "utils.h"
#include "motors.h"
#include "kinematics.h"
#include "line_sensors.h"
#include "irproximity.h"
#include "mapping.h"
#include "RF_Interface.h"
#include <Wire.h>
#include "imu.h"
#include "magnetometer.h"
#include "Pushbutton.h"
#include "control.h"
#include "move.h"
#include "Location2D.h"

 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  * Definitions.  Other definitions exist in the .h files above.                  *
  * Also ensure you check pins.h for pin/device definitions.                      *
  *                                                                               *
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#define BAUD_RATE 9600

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Class Instances.                                                              *
   * This list is complete for all devices supported in this code.                 *
   *                                                                               *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
Kinematics    Pose; //Kinematics class to store position and heading

//LineSensor    LineLeft(LINE_LEFT_PIN);		//Left line sensor
LineSensor    LineCentre(LINE_CENTRE_PIN);	//Centre line sensor
//LineSensor    LineRight(LINE_RIGHT_PIN);	//Right line sensor

SharpIR       cnr_IR_sensor(SHARP_IR_CENTER_PIN,8,25); //Distance sensor
SharpIR       lft_IR_sensor(SHARP_IR_LEFT_PIN, 10, 20); //Distance sensor Left
SharpIR       rgt_IR_sensor(SHARP_IR_RIGHT_PIN, 10, 20); //Distance sensor Right


Imu           Imu;
//Magnetometer  Mag; // Class for the magnetometer

Motor         left_motor(MOTOR_PWM_L, MOTOR_DIR_L);
Motor         right_motor(MOTOR_PWM_R, MOTOR_DIR_R);

//These work for our Romi - We strongly suggest you perform your own tuning
PID           LeftSpeedControl(3.5, 20.9, 0.04);
PID           RightSpeedControl(3.5, 20.9, 0.04);

//PID           LeftSpeedControl(3.5, 20.9, 0.0);
//PID           RightSpeedControl(3.5, 20.9, 0.0);
PID           HeadingControl(1.5, 0, 0.001);

Mapper        Map; //Class for representing the map

Pushbutton    ButtonB(BUTTON_B, DEFAULT_STATE_HIGH);

extern RFID rfid;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Global variables.                                                             *
 * These global variables are not mandatory, but are used for the example loop() *
 * routine below.                                                                *
 *                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 //Use these variables to set the demand of the speed controller
//bool use_speed_controller = true;
//float left_speed_demand = 0;
//float right_speed_demand = 0;

int FSM_state;

float forward_bias;
float turn_bias;

//CheckNoMovement Variable
long noMovement = millis();
long last_checked_if_I_was_on_line_time = millis();
long last_object_was_visible_time = millis();
//long delay_int = 2000;

//NavPoints for bug algorithm
Location2D navPoints[NAV_POINTS];
int navPoinIndex = 0;

float x_target = 160;
float y_target = 160;

float x_before_nav;
float y_before_nav;

unsigned long last_time;
unsigned long curr_time;

// Initialize selected PID
volatile int selected_pid = NO_PID;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This setup() routine initialises all class instances above and peripherals.   *
 * It is recommended:                                                            *
 * - You keep this sequence of setup calls if you are to use all the devices.    *
 * - Comment out those you will not use.                                         *
 * - Insert new setup code after the below sequence.                             *
 *                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void setup(){

	// These two function set up the pin
	// change interrupts for the encoders.
	setup_encoder_rgt_1();
	setup_encoder_lft_0();
	cli();
	setup_timer_3(100);
	//setup_timer_4(5);
	sei();

	//Set speed control maximum outputs to match motor
	LeftSpeedControl.setMax(100);
	RightSpeedControl.setMax(100);

	//Setup RFID card
	setupRFID();

	// These functions calibrate the IMU and Magnetometer
	// The magnetometer calibration routine require you to move
	// your robot around  in space.  
	// The IMU calibration requires the Romi does not move.
	// See related lab sheets for more information.
	/*
	Wire.begin();
	Mag.init();
	Mag.calibrate();
	Imu.init();
	Imu.calibrate();
	*/

	// Set the random seed for the random number generator
	// from A0, which should itself be quite random.
	randomSeed(analogRead(A0));
	//...

	// Initialise Serial communication
	Serial.begin(BAUD_RATE);
	delay(1000);
	//Serial.println("Board Reset");

	// Romi will wait for you to press a button and then print
	// the current map.
	//
	// !!! A second button press will erase the map !!!
	ButtonB.waitForButton();

	Map.printMap();
	// Watch for second button press, then begin autonomous mode.
	ButtonB.waitForButton();

	//Serial.println("Map Erased - Mapping Started");
	Map.resetMap();

	// Your extra setup code is best placed here:
	// ...
	// ...
	// but not after the following:

	// Because code flow has been blocked, we need to reset the
	// last_time variable of the PIDs, otherwise we update the
	// PID with a large time elapsed since the class was 
	// initialised, which will cause a big intergral term.
	// If you don't do this, you'll see the Romi accelerate away
	// very fast!
	LeftSpeedControl.reset();
	RightSpeedControl.reset();

	// Initialize control information
	initialize_controls();

	FSM_state = STATE_INITIAL;

	createNavList();

	//No Movement Function Variable
	noMovement = millis();

	last_time = millis();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This loop() demonstrates all devices being used in a basic sequence.
 * The Romi should:
 * - move forwards with random turns
 * - log lines, RFID and obstacles to the map.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void loop() {

	switch (FSM_state) {

		case STATE_INITIAL: {
			initializing_beeps();
			break;
		}
		case STATE_MOVE_TO_POSITION: {
			// calculate where in the map I want to go - move until the central IR finds an obstacle forward
			x_target = getNavPoint(navPoinIndex).getX();
			y_target = getNavPoint(navPoinIndex).getY();
			move_to_pos(x_target, y_target);

			if (move_status() == MOVE_ENDED) {
				//Serial.println("[STAT_MOVE_POS]:: Move Ended");
				halt_movement();
				play_tone(10);
				play_tone(10);
				Pose.print_TM();

				FSM_state = STATE_REACHED_POINT;
			}

			if (object_within_range_of_IR_sensor(cnr_IR_sensor, 6, 12)) {
				//Serial.println("[STAT_MOVE_POS]::Object Detected Center!");
				halt_movement();

				FSM_state = STATE_TURN_TOWARDS_OBSTACLE;
				record_last_trajectory_location();
				play_tone(10);
			}

			if (object_within_range_of_IR_sensor(lft_IR_sensor, 6, 8)) {
				//Serial.println("[STAT_MOVE_POS]::Object Detected Left!");
				halt_movement();
				play_tone(10);

				FSM_state = STATE_NAVIGATE_OBSTACLE_II;
				record_last_trajectory_location();
				reset_pid(SPEED_ENC);
				last_checked_if_I_was_on_line_time = millis();
				last_object_was_visible_time = millis();
			}

			isOnLine(Pose.get_x_cm(), Pose.get_y_cm(), x_target, y_target);
			deadlock_check();
			//update_kinematics_with_SML();
			break;
		}
		case STATE_TURN_TOWARDS_OBSTACLE: {

			//specify a turn angle
			turn_to_angle(65, DIR_TURN_RIGHT);

			if (move_status() == MOVE_ENDED) {
				//Serial.println("[STAT_TRN_TOW_OBST]:: Move Ended");
				halt_movement();
				play_tone(10);
				Pose.print_TM();

				FSM_state = STATE_NAVIGATE_OBSTACLE_I;
				last_checked_if_I_was_on_line_time = millis();
				last_object_was_visible_time = millis();
			}

			deadlock_check();
			//update_kinematics_with_SML();
			break;
		}
		case STATE_NAVIGATE_OBSTACLE_I: {
			//Serial.println("Navigate Obstacle I: Turn away from the obstacle");

			//specify a turn angle
			turn_to_angle(25, DIR_TURN_RIGHT);

			// If an obstacle is found on the left, proceed to navigate around.
			if (object_within_range_of_IR_sensor(lft_IR_sensor, 6, 17)) {
				//Serial.println("[NAV_OBST_I]:: Left Sensor saw wall");
				halt_movement();
				play_tone(255);
				Pose.print_TM();


				FSM_state = STATE_NAVIGATE_OBSTACLE_II;
				//record_last_trajectory_location();
				reset_pid(SPEED_ENC);
				last_checked_if_I_was_on_line_time = millis();
				last_object_was_visible_time = millis();
			}

			// if the robot did a full 90 degrees and no obstacle seen, stop
			if ((move_status() == MOVE_ENDED)) {
				//Serial.println("[NAV_OBST_I]: Turn Ended");
				halt_movement();
				play_tone(10);
				play_tone(10);

				FSM_state = STATE_CONTINUE_TO_POSITION;
				reset_pid(SPEED_ENC);
			}

			deadlock_check();
			//update_kinematics_with_SML();
			break;
		}
		case STATE_NAVIGATE_OBSTACLE_II: {
			//Serial.println("[NAV_OBST_II]: Follow obstacle");

			forward_bias = TARGET_SPEED;

			// lft_IR_sensor.print_IR_measurement();
			if (object_within_range_of_IR_sensor(lft_IR_sensor, 0, 16)) { //16
				turn_bias = 100 / lft_IR_sensor.get_distance_in_cm();
				last_object_was_visible_time = millis();
			}
			else { //17
				if ((millis() - last_object_was_visible_time) > (PTIMEOUT / 8)){
					turn_bias = -70 / lft_IR_sensor.get_distance_in_cm();
				}
			}

			selected_pid = SPEED_ENC;
			target_speed_left = (forward_bias + turn_bias);
			target_speed_right = (forward_bias - turn_bias);

			// If object continue to not be visible...
			if ((millis() - last_object_was_visible_time) > 1 * PTIMEOUT) {
				//Serial.println("[NAV_OBST_II]::Object Lost");
				Pose.print_TM();
				halt_movement();
				play_tone(10);
				play_tone(10);
				play_tone(10);

				FSM_state = STATE_NAVIGATE_OBSTACLE_III;
			}

			if (isOnLine(Pose.get_x_cm(), Pose.get_y_cm(), x_target, y_target)) {
				//Serial.println("[NAV_OBST_II]::Aligned with Intersecting Line");
				halt_movement();
				play_tone(10);
				play_tone(10);

				// Continue moving to position....
				FSM_state = STATE_CONTINUE_TO_POSITION;
				last_checked_if_I_was_on_line_time = millis();

				// Unless the position I am currently in is the same I intitially started with ...
				// In that case I have done a circle around the obstacle and couldn't reach the target
				compare_with_last_trajectory_location(Pose.get_x_cm(), Pose.get_y_cm());
			}

			//update_kinematics_with_SML();
			deadlock_check();
			break;
		}
		case STATE_NAVIGATE_OBSTACLE_III: {
			//Serial.println("Navigate Obstacle III: Move 5cm or until the left sensor sees the obstacle again");

			move_straight(5);

			if ((move_status() == MOVE_ENDED)) {
				//Serial.println("[NAV_OBST_III]:: Move Ended ");
				halt_movement();
				play_tone(10);
				play_tone(10);

				FSM_state = STATE_NAVIGATE_OBSTACLE_IV;
			}

			deadlock_check();
			//update_kinematics_with_SML();
			break;
		}
		case STATE_NAVIGATE_OBSTACLE_IV: {

			turn_to_angle(90, DIR_TURN_LEFT);

			// If an obstacle is found on the left, I haven't finished going around yet.
			if (object_within_range_of_IR_sensor(lft_IR_sensor, 0, 8)) {
				//Serial.println("[NAV_OBST_V]:: Obstacle Found Again");
				halt_movement();
				play_tone(10);

				FSM_state = STATE_NAVIGATE_OBSTACLE_II;
				//record_last_trajectory_location();
				last_object_was_visible_time = millis();
				last_checked_if_I_was_on_line_time = millis();
			}

			// If I turned 90 degrees and nothing was found, I navigated the obstacle
			if ((move_status() == MOVE_ENDED)) {
				//Serial.println("[NAV_OBST_V]:: Move Ended ");
				halt_movement();
				play_tone(10);
				play_tone(10);

				FSM_state = STATE_CONTINUE_TO_POSITION;
			}

			//update_kinematics_with_SML();
			deadlock_check();
			break;
		}

		case STATE_CONTINUE_TO_POSITION: {
			//Serial.println("[STAT_CONT]:: Continue to Position");
			Pose.print_TM();
			// calculate the new parameters to move to the desired position
			// call the move to position state 
			halt_movement();
			FSM_state = STATE_MOVE_TO_POSITION;
			break;
		}
		case STATE_REACHED_POINT: {
			// re-initializations
			
			navPoinIndex++;
			if (navPoinIndex < NAV_POINTS) {
				FSM_state = STATE_MOVE_TO_POSITION;
			}
			else {
				FSM_state = STATE_END;
			}
			
			break;
		}
		case STATE_END: {
			play_tone(255);
			play_tone(255);
			play_tone(255);
			break;
		}
		case STATE_180: {

			turn_to_angle(180, DIR_TURN_RIGHT);

			// If I turned 90 degrees and nothing was found, I navigated the obstacle
			if ((move_status() == MOVE_ENDED)) {
				//Serial.println("[STATE_180]:: Navigate obstacle");
				halt_movement();
				play_tone(10);

				FSM_state = STATE_NAVIGATE_OBSTACLE_II;
				//record_last_trajectory_location();
				last_object_was_visible_time = millis();
				last_checked_if_I_was_on_line_time = millis();
			}

			//update_kinematics_with_SML();
			break;
		}
	}
	
    //object_within_range_of_IR_sensor(lft_IR_sensor, 0, 16);
	//print_IR_distances();

	//doMovement();
	doMapping();

	// Update Kinematics 
	//Pose.update(count_e0, count_e1);			// Solution without Instanteneous centre of curvature 
	//Pose.update_ICC(count_e0, count_e1, getEnc0Speed(), getEnc1Speed(), (curr_time - last_time)*0.001); // ICC but based on fixed speeds

	//update_kinematics_with_ICC();
	
	update_kinematics_with_SML();
	delay(1);
}

void update_kinematics_with_ICC() {
	curr_time = millis();
	Pose.update_ICC_enc_counts(count_e0, count_e1, (curr_time - last_time)*0.001);		// ICC with calculated speeds on the spot
	last_time = curr_time;
	Pose.print_TM();

	//Serial.println(count_e0);
	//Serial.println(count_e1);
}

void update_kinematics_with_SML() {
	Pose.update(count_e0, count_e1);
}

bool deadlock_check() {

	if (object_within_range_of_IR_sensor(lft_IR_sensor, 0, 10) && object_within_range_of_IR_sensor(cnr_IR_sensor, 0, 10) && object_within_range_of_IR_sensor(rgt_IR_sensor, 0, 10)) {
		
		//Serial.println("[STATE_ANY]:: Deadlocked state");
		halt_movement();
		play_tone(10);
		play_tone(10);
		play_tone(10);
		play_tone(10);

		FSM_state = STATE_180;
		return true;
	}
	else {
		return false;
	}
}

void record_last_trajectory_location() {
	x_before_nav = Pose.get_x_cm();
	y_before_nav = Pose.get_y_cm();
}

void compare_with_last_trajectory_location(float x_curr, float y_curr) {

	// Obstacle Navigation brought me back to where I started - need to set another course 
	// If the current position is very close (1cm difference) to where I originally started navigating the obstacle 
	// - that means that I did a circle and I wasn't able to find the end point - therefore is inside the obstacle area.
	if ((abs(x_curr - x_before_nav) < 2) && (abs(y_curr - y_before_nav) < 2)) {
		
		halt_movement();
		play_tone(10);
		play_tone(10);
		play_tone(10);
		play_tone(10);
		FSM_state = STATE_REACHED_POINT;
	}
}



void initializing_beeps() {

	play_tone(255);
	delay(1000);
	play_tone(255);
	delay(1000);

	// Line Sensor calibration
	LineCentre.calibrate();
	//LineLeft.calibrate();
	//LineRight.calibrate();

	// Set thresholds here 
	//LineLeft.set_threshold(300);			
	//LineCentre.set_threshold(280);
	//LineRight.set_threshold(260);

	// reset PID controls
	reset_controls();

	FSM_state = STATE_MOVE_TO_POSITION;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * We have implemented a random walk behaviour for you
 * with a *very* basic obstacle avoidance behaviour.
 * It is enough to get the Romi to drive around.  We
 * expect that in your first week, should should get a
 * better obstacle avoidance behaviour implemented for
 * your Experiment Day 1 baseline test.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void doMovement() {

	// Static means this variable will keep
	// its value on each call from loop()
	static unsigned long walk_update = millis();

	// used to control the forward and turn
	// speeds of the robot.
	float forward_bias;
	float turn_bias;

	// Check if we are about to collide.  If so,
	// zero forward speed
	if (cnr_IR_sensor.getDistanceRaw() > 450) {
		forward_bias = 0;
	}
	else {
		forward_bias = 5;
	}

	// Periodically set a random turn.
	// Here, gaussian means we most often drive
	// forwards, and occasionally make a big turn.
	if (millis() - walk_update > 500) {
		walk_update = millis();

		// randGaussian(mean, sd).  utils.h
		turn_bias = randGaussian(0, 6.5);

		// Setting a speed demand with these variables
		// is automatically captured by a speed PID 
		// controller in timer3 ISR. Check interrupts.h
		// for more information.
		selected_pid = SPEED_ENC;
		target_speed_left = (forward_bias + turn_bias);
		target_speed_right = (forward_bias - turn_bias);
		//left_speed_demand = forward_bias + turn_bias;
		//right_speed_demand = forward_bias - turn_bias;
	}
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This function groups up our sensor checks, and then
 * encodes into the map.  To get you started, we are
 * simply placing a character into the map.  However,
 * you might want to look using a bitwise scheme to
 * encode more information.  Take a look at mapping.h
 * for more information on how we are reading and
 * writing to eeprom memory.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void doMapping() {

	// Read the IR Sensor and determine distance in
	// mm.  Make sure you calibrate your own code!
	// We threshold a reading between 40mm and 12mm.
	// The rationale being:
	// We can't trust very close readings or very far.
	// ...but feel free to investigate this.

	/*	Map according to Central IR Sensor
	*/
	float distance = cnr_IR_sensor.get_distance_in_mm();
	if (distance < 160 && distance > 0) {

		// We know the romi has the sensor mounted
		// to the front of the robot.  Therefore, the
		// sensor faces along Pose.Theta.
		// We also add on the distance of the 
		// sensor away from the centre of the robot.
		distance += 80;

		// Here we calculate the actual position of the obstacle we have detected
		float projected_x = Pose.get_x_mm() + (distance * cos(Pose.get_theta_radians()));
		float projected_y = Pose.get_y_mm() + (distance * sin(Pose.get_theta_radians()));
		Map.updateMapFeature((byte)'O', projected_x, projected_y);
		Serial.println("Mapped Obstacle Center");
	}

	/*	Map according to Left IR Sensor
	*/
	distance = lft_IR_sensor.get_distance_in_mm();

	if (distance < 160 && distance > 12) {

		// TODO:: Measure correct distance
		distance += 80;

		// TODO:: Specify theta values (standard, should be derived by the sensor placement)
		float left_IR_theta = 65*PI/180;

		float projected_x = Pose.get_x_mm() + (distance * cos(Pose.get_theta_radians()) + left_IR_theta);
		float projected_y = Pose.get_y_mm() + (distance * sin(Pose.get_theta_radians()) + left_IR_theta);
		Map.updateMapFeature((byte)'O', projected_x, projected_y);
		Serial.println("Mapped Obstacle Left");

	}

	/*	Map according to Right IR Sensor
	*/
	/*distance = rgt_IR_sensor.get_distance_in_mm();

	if (distance < 40 && distance > 12) {

		// TODO:: Measure correct distance
		distance += 80;

		// TODO:: Specify theta values (standard, should be derived by the sensor placement)
		float right_IR_theta = 0;

		float projected_x = Pose.get_x_mm() + (distance * cos(Pose.get_theta_radians()) - right_IR_theta);
		float projected_y = Pose.get_y_mm() + (distance * sin(Pose.get_theta_radians()) - right_IR_theta);
		Map.updateMapFeature((byte)'O', projected_x, projected_y);
		Serial.println("Mapped Obstacle Right");
	}*/

	// Check RFID scanner.
	// Look inside RF_interface.h for more info.
	if (checkForRFID()) {

		//Serial.println("FoundRFID");
		// Add card to map encoding.  
		Map.updateMapFeature((byte)'R', Pose.get_y_mm(), Pose.get_x_mm());
		//Serial.println("Mapped RFID");

		// you can check the position reference and
		// bearing information of the RFID Card in 
		// the following way:
		//Serial.println(serialToBearing( rfid.serNum[0] ));
		//Serial.println(serialToXPos( rfid.serNum[0] ));
		//Serial.println(serialToYPos( rfid.serNum[0] ));
		//
		// Note, that, you will need to set the x,y 
		// and bearing information in rfid.h for your
		// experiment setup.  For the experiment days,
		// we will tell you the serial number and x y 
		// bearing information for the cards in use.  

	}

	// Basic uncalibrated check for a line.
	// Students can do better than this after CW1 ;)
	if (LineCentre.readRaw() > 580) {
		Map.updateMapFeature((byte)'L', Pose.get_y_mm(), Pose.get_x_mm());
		//Serial.println("Mapped Line");
	}
}

void halt_movement() {

	reset_move_states();
	reset_pid(selected_pid);
	selected_pid = NO_PID;
	killMotors();											// Stops Motors && resets PID && selected_PID = NO_PID
	last_checked_if_I_was_on_line_time = millis();			// delay for the check line call
	reset_pid(SPEED_ENC);
}

void print_IR_distances() {
	Serial.print("\t > Center IR:: Distance:");
	Serial.print(cnr_IR_sensor.get_distance_in_cm());
	Serial.print(" || Left IR:: Distance:  ");
	Serial.println(cnr_IR_sensor.get_distance_in_cm());
}

bool isOnLine(float x, float y, float x_target, float y_target) {
	
	if ((millis() - last_checked_if_I_was_on_line_time) > 2*PTIMEOUT) {

		float slope = (x_target - x) / (y_target - y);
		float slope_target = (x_target - 90) / (y_target - 90);
		float error = abs(slope - slope_target);
		/*Serial.print("Slope target is: ");
		Serial.println(slope_target);
		Serial.print("Slope is: ");
		Serial.println(slope);
		Serial.print("Error: ");
		Serial.println(error);
		*/
		if (error < 0.1) {
			play_tone(255);
			return true;
		}
		else {
			return false;
		}
	} 
	else {
		return false;
	}
}

/* void setup_timer_4(int hertz)
	Routine to setup timer4 with a specified frequency
*/
void setup_timer_4(int hertz) {

	// disable global interrupts
	//cli();

	// Reset timer2 to a blank condition.
	// TCCR = Timer/Counter Control Register
	//TCCR4A = 0; // set entire TCCR2A register to 0
	//TCCR4B = 0; // same for TCCR2B

	TCNT4 = 0;  //initialize counter value to 0

	// First, turn on CTC mode.  Timer 4 will count up
	// and create an interrupt on a match to a value.
	// See table 14.4 in manual, it is mode 4.
	//TCCR4B = TCCR4B | (1 << WGM32);

	// In order to set prescaler and counter value, the frequency should divide the
	// clock_frequency % timer_frequency*LED_frequency = 0 (or preferably the minimum).

	int prescaler[] = {16384, 8192, 4096, 2048, 1024};
	int minrem = 10000;
	int minremi = 4;
	double remainder;

	for (int i = 0; i < 4; i++) {
		remainder = 64000000 % (prescaler[i] * hertz);
		Serial.print(prescaler[i]);
		if (remainder == 0) {
			minremi = i;
			break;
		}
		else if (remainder <= minrem) {
			minrem = remainder;
			minremi = i;
			break;
		}
	}
	Serial.print("Timer 4:: Best prescaling at:");
	Serial.print(prescaler[minremi]);
	Serial.println("\n");

	switch (prescaler[minremi]) {
		//case 16384: { TCCR4B = TCCR4B | (1 << CS40) | (1 << CS41) | (1 << CS42) | (1 << CS43); break; }
		//case 8192:  { TCCR4B = TCCR4B | (1 << CS41) | (1 << CS42) | (1 << CS43); break; }
		//case 4096:  { TCCR4B = TCCR4B | (1 << CS40) | (1 << CS42) | (1 << CS43); break; }
		//case 2048:  { TCCR4B = TCCR4B | (1 << CS42) | (1 << CS43); break; }
		case 1024:  { TCCR4B = TCCR4B | (1 << CS40) | (1 << CS41) | (1 << CS43);  break; }
	default: { TCCR4B = TCCR4B | (1 << CS40) | (1 << CS41) | (1 << CS43); break; }
	}
	// Set Compare Match counter value
	//OCR4A = ((16000000 / (prescaler[minremi])) / hertz);
	OCR4A = ((16000000 / 1024) / hertz);

	// enable timer compare interrupt:
	TIMSK4 = TIMSK4 | (1 << OCIE4A);

	// enable global interrupts:
	//sei();
}

/* ISR(TIMER4_COMPA_vect)
	ISR Routine that updates the Kinematics 
*/
ISR(TIMER4_COMPA_vect) {

	Serial.println(curr_time);
	curr_time = millis();
	Pose.update_ICC_enc_counts(count_e0, count_e1, (curr_time - last_time)*0.001);
	last_time = curr_time;
	Pose.print_TM();

}

