#include "irproximity.h"


extern SharpIR       cnr_IR_sensor;
extern SharpIR       lft_IR_sensor;
extern SharpIR       rgt_IR_sensor;

/*
 *  This is quite a computationally expensive routine,
 *  so you might want to consider not using it.  But 
 *  gaussian random numbers are really nice for a random
 *  walk behaviour :)
 *  From: http://www.taygeta.com/random/gaussian.html
 */
float randGaussian( float mean, float sd ) {
   float x1, x2, w, y;
   
   do {
     // Adaptation here because arduino random() returns a long
     x1 = random(0,2000) - 1000;
     x1 *= 0.001;
     x2 = random(0,2000) - 1000;
     x2 *= 0.001;
     w = (x1 * x1) + (x2 * x2); 
     
   } while( w >= 1.0 );
   
   w = sqrt( (-2.0 * log( w ) )/w );
   y = x1 * w;
   
   return mean + y * sd;
}
 
void play_tone(int volume) {

	int i;
	for (i = 0; i < 2; i++) {
		analogWrite(6, volume);
		delay(100);
		analogWrite(6, 0);
	}
}

bool object_within_range_of_IR_sensor(SharpIR snsr, int min, int max) {

	float distance = snsr.get_distance_in_cm();
	//Serial.println("The distance is:::::");
	//Serial.println(distance);


	if ((distance < max) && (distance > min)) {
		return true;
	}
	else {
		return false;
	}
}


void print_IR_states() {

	Serial.print("> Center Sensor :: ");
	Serial.print("Raw ");
	Serial.println(cnr_IR_sensor.getDistanceRaw());
	Serial.print("Distance :  ");
	Serial.print(cnr_IR_sensor.getDistanceRaw());

	Serial.print("> Left Sensor :: ");
	Serial.print("Raw ");
	Serial.println(lft_IR_sensor.getDistanceRaw());
	Serial.print("Distance :  ");
	Serial.println(lft_IR_sensor.get_distance_in_cm());

	Serial.print("> Right Sensor :: ");
	Serial.print("Raw ");
	Serial.println(rgt_IR_sensor.getDistanceRaw());
	Serial.print("Distance :  ");
	Serial.println(rgt_IR_sensor.get_distance_in_cm());
	Serial.println("> ----------------------------------------- < ");

}