#pragma once

// Romi Characteristics
#define degreesToRadians(angleDegrees) (angleDegrees * M_PI / 180.0)
#define radiansToDegrees(angleRadians) (angleRadians * 180.0 / M_PI)

#define CM_PER_COUNT 0.015
#define COUNTS_PER_CM 67
#define ENC_TO_CM	0.015
#define WHEEL_DIAMETER 7.2
#define WHEEL_DISTANCE  14.5

// Action LIST 
#define DIR_FORW		0
#define DIR_BCKW		1
#define DIR_TURN_LEFT	2
#define DIR_TURN_RIGHT	3
#define FOLLOW_LINE		4
#define LINE_END		5

// PID LIST
#define NO_PID			0
#define POSITION_PID	1
#define SPEED_PID		2
#define LINE_PID		3
#define ANGLE_PID		4
#define IR_PID			5
#define SPEED_ENC		6
#define IR_OBSTACLE_PID 7

// ERROR METRICS
#define SPEED_ERROR 1
#define SPEED_ERROR_CMS 0.1	//cm/s
#define POSITION_ERROR 10

// Definitions
#define TARGET_SPEED 7.0			// Base target speed for Line PID


#define LOOP_DELAY 1
#define LONG_DELAY 100


// FSM States
#define STATE_INITIAL 1
#define STATE_MOVE_TO_POSITION 2
#define STATE_NAVIGATE_OBSTACLE_I 3			// turn a specific angle
#define STATE_NAVIGATE_OBSTACLE_II 4		// follow obstacle
#define STATE_NAVIGATE_OBSTACLE_III 5		// if lost, turn until obstacle is found
#define STATE_NAVIGATE_OBSTACLE_IV 6		// break ?
#define STATE_CONTINUE_TO_POSITION 7
#define STATE_REACHED_POINT 8
#define STATE_END 9
#define STATE_TURN_TOWARDS_OBSTACLE 10
#define STATE_180 11


// Move States 
#define MOVE_SET_PARAMS 1
#define MOVE_1 2
#define MOVE_1_IN_PROCESS 3
#define MOVE_2 4
#define MOVE_2_IN_PROCESS 5
#define MOVE_ENDED 6


#define TMAX 1
#define TMIN 0

#define NAV_POINTS 3


#define PTIMEOUT 2000 // timeout in milliseconds