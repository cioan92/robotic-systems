// control.h
#ifndef _CONTROL_h
#define _CONTROL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "definitions.h"
#include "pins.h"
#include "pid.h"
#include "motors.h"
#include "line_sensors.h"
#include "irproximity.h"
#include "kinematics.h"

/* ---------------------------------- PID Control ---------------------------------------*/
// Position PID
extern PID leftPose;
extern PID rightPose;

// Angle PID
extern PID leftAngle;
extern PID rightAngle;

// Speed PID
extern PID leftSpeed;
extern PID rightSpeed;

// IR follow PID
extern PID leftIR;
extern PID rightIR;

// Speed-Angle PID 
// (Speed is adjusted based on changes in angle)
extern PID leftSpeedA;
extern PID rightSpeedA;

// Line Following PID
//extern PID lineLeftSpeed;
//extern PID lineRightSpeed;

// Model CW2 solution
extern PID LeftSpeedControl;
extern PID RightSpeedControl;

/* ----------------------------------------------------------------------------------*/
//
extern bool enable_pos_PID;
extern bool enable_spd_PID;
extern bool enable_line_PID;

extern float outputI, outputII, outputIII, outputIV;

/* Function Declaration */
void refresh_pid_speed(bool feedback);
void refresh_pid_position(bool feedback);
void refresh_pid_angle(bool feedback);
void refresh_pid_line(bool feedback);
void refresh_pid_IR(bool feedback);
double line_orientation();
void choose_PID_controller(int choice, bool feedback);
void reset_controls();
void reset_pid(int choice);
void initialize_controls();

/* External Definitions */
extern Motor left_motor;
extern Motor right_motor;
//extern LineSensor LineLeft;
extern LineSensor LineCentre;
//extern LineSensor LineRight;
extern SharpIR  lft_IR_sensor;
extern Kinematics Pose; 

extern int moveType;
extern long target_left, target_right;
extern float target_speed_left, target_speed_right;
extern float target_angle;

extern volatile long count_e0;
extern volatile long count_e1;

extern volatile float e0_enc_speed;
extern volatile float e1_enc_speed;

extern double getEnc1Speed();
extern double getEnc0Speed();
#endif

