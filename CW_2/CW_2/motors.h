// motors.h
#ifndef _MOTORS_h
#define _MOTORS_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "definitions.h"

class Motor {
public:
	//Motor();
	Motor(byte pwm, byte dir);

	void setSpeed(int demand);
	int getSpeed();
	void setMaxSpeed(int newMax);
	void setPins(int pwm, int dir);
	void setDebug(bool state);
	void stop();
	int Motor::getEncRotation(int deg_angle);
	int Motor::getEncDistance(float distance);
	int Motor::setDirection(int dir);
	void Motor::setPower(float demand);

private:

	int pwm_pin;
	int dir_pin;
	int direction;			// 0: Forward , 1:backward
	int maxSpeed = 255;		// Max Speed 
	int speed;				// Absolute speed value
	bool debug = false;
};

#endif

