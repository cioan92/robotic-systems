// move.h

#include "definitions.h"
#include "pins.h"
#include "pid.h"
#include "motors.h"
#include "line_sensors.h"
#include "irproximity.h"
#include "kinematics.h"
#include "Location2D.h"

extern Motor left_motor;
extern Motor right_motor;

extern Kinematics Pose;
extern PID leftAngle;
extern PID rightAngle;
extern PID leftPose;
extern PID rightPose;

extern int moveType;
extern long target_left, target_right;
extern float target_speed_left, target_speed_right;
extern float target_angle;
extern float rotation_angle;
extern double cm_straight;

extern void choose_PID_controller(int choice, bool feedback);
extern void refresh_pid_position(bool feedback);

extern volatile long count_e0;
extern volatile long count_e1;

extern double getEnc1Speed();
extern double getEnc0Speed();
extern void reset_pid(int choice);

extern volatile int selected_pid;

extern long noMovement;


extern Location2D navPoints[NAV_POINTS];

/* --------------------- Function Declarations ---------------------------- */
// Non-Blocking Functions
void perform_move(double trgt, double trgt_speed, int move_type);

bool checkIfMoveFinished();
bool checkNoMovement();

//set both motors speed to 0
void killMotors();

void move_to_pos(double x, double y);
void move_straight(double cm_straight);
void turn_to_angle(float rotation_angle, int direction);
void move_to_target(double cm_straight);
void turn_to_target(float theta, int direction);
void reset_move_states();
int move_status();

void createNavList();
Location2D* getNavList();
Location2D getNavPoint(int index);