#include "line_sensors.h"


LineSensor::LineSensor(int line_pin){
	pin = line_pin;
	pinMode(pin, INPUT);
}


int LineSensor::readRaw(){
	return analogRead(pin);
}


void LineSensor::calibrate(){
	analogWrite(BUZZER_PIN, 255);

	for (int i = 0; i < NUM_CALIBRATIONS; i++)
	{
		calibration_offset += analogRead(pin);
		delay(10);
	}

	calibration_offset = calibration_offset / NUM_CALIBRATIONS;

	analogWrite(BUZZER_PIN, 0);

}

float LineSensor::read_calibrated(){
	
	return (analogRead(pin) - calibration_offset);
}


void LineSensor::setThreshHold(float value) {
	this->threshHold = value;

}

float LineSensor::getThreshHold() {
	return this->threshHold;


}

bool LineSensor::isBlack() {
	if (read_calibrated() > threshHold) {
		return true;
	}
	else {
		return false;
	}
}

//returns false if nothing needs to happen
//returns true if robot is outside the map..and actions are happening (eg rotation,move)
// if true is returned.. all moves happening need to stop 
bool LineSensor::keepMeIn() {
	
	if (isBlack() || goInside) {
		if (goInside == false) {
			killMotors();
			perform_move(180, 0, DIR_TURN_LEFT);
			selected_pid = ANGLE_PID;
		}
		goInside = true;
		
		

	  
	   if (rotationFinished && straightMoveFinished) {
		   selected_pid = SPEED_PID;
		   perform_move(5, 10, DIR_FORW);
		   straightMoveFinished = false;
	   }
	   else {
		   rotationFinished = checkIfMoveFinished();
	   }
	   if (straightMoveFinished == false) {
		  
		   goInside = !checkIfMoveFinished();
		   straightMoveFinished = !goInside;
	   }

		
		return goInside;
	}
	else {
		
		return goInside;
	}
	
}

void LineSensor::printDebug() {
	if (lineSensorDebug) {
		if (millis() - elapsed > 1000) {


			Serial.println("----------------------------------------------");


			Serial.println("-----LineSensorDebug-----");
			Serial.print("Line Value: ");
			Serial.print(read_calibrated());
			Serial.println();
			Serial.print("GoInside Value: ");
			Serial.print(goInside);
			Serial.println();
			Serial.print("StraightMovement Value: ");
			Serial.print(straightMoveFinished);
			Serial.println();
			Serial.print("Rotation Value: ");
			Serial.print(rotationFinished);
			Serial.println();
			Serial.print("Left Targer: ");
			Serial.print(target_left);
			Serial.println();
			Serial.print("Count left: ");
			Serial.print(count_e0);
			Serial.println();
			Serial.println("----------------------------------------------");
			elapsed = millis();
		}
	}
}