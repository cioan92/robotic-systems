#ifndef _IRProximity_h
#define _IRProximity_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "definitions.h"

//#define IR_PIN A0
#define IR_calibration_IRA0(raw) (74398 * pow(raw, -1.379))
#define IR_calibration_IRA1(raw) (39672 * pow(raw, -1.256))		// Calibration for Left IR 
//#define IR_calibration_IRA1(raw) (61757 * pow(raw, -1.35))		// Calibration for Left IR 
#define IR_calibration_IRA11(raw) (46872 * pow(raw, -1.307))	// Calibration for Right IR

class SharpIR{
    public:
        SharpIR(byte _pin, int threshold_min, int threshold_max);
		int IR_raw;
		double IR_calibr;

        int  getDistanceRaw();
		float get_distance_in_cm();
		float get_distance_in_mm();
        //void calibrate();
		void print_IR_measurement();
		bool object_detected(int t_type);
		void set_IR_threshold(int value, int  t_type);
		int get_IR_threshold(int t_type);
		bool object_within_range(int min, int max);

    private:
        byte pin;
		int distance_threshold_min, distance_threshold_max;
};

#endif
