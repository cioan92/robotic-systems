// encoder.h
#include <math.h>
#include "kinematics.h"

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define E1_A_PIN  7
#define E1_B_PIN  23
#define E0_A_PIN  26
#define ENC_DIST 0.015			// 1 encoder count translates to 0.015 cm 

// Volatile Global variables used by Encoder ISR.
extern volatile long count_e1; // used by encoder to count the rotation
extern volatile bool oldE1_A;  // used by encoder to remember prior state of A
extern volatile bool oldE1_B;  // used by encoder to remember prior state of B

extern volatile long count_e0; // used by encoder to count the rotation
extern volatile bool oldE0_A;  // used by encoder to remember prior state of A
extern volatile bool oldE0_B;  // used by encoder to remember prior state of B

extern volatile unsigned long e0_interval;
extern volatile unsigned long e0_last_time;
extern volatile unsigned long e1_interval;
extern volatile unsigned long e1_last_time;
extern volatile float e0_speed;
extern volatile float e1_speed;
extern volatile long e0_last_count;
extern volatile long e1_last_count;
extern volatile long e0_speed_;
extern volatile long e1_speed_;
extern volatile bool timer_hit;
extern volatile int selected_pid;

extern volatile float e0_enc_speed;
extern volatile float e1_enc_speed;

extern Kinematics    Pose;

extern void choose_PID_controller(int choice, bool feedback);

ISR(INT6_vect);
ISR(PCINT0_vect);
ISR(TIMER3_COMPA_vect);

void setup_encoder_rgt_1();
void setup_encoder_lft_0();
void print_encoder_status();
void print_motor_speed();
void setup_timer_3(int hertz);
double getEnc0Speed();
double getEnc1Speed();
