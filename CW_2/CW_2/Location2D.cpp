#include "Location2D.h"


Location2D::Location2D() {
	this->x = 0;
	this->y = 0;
	this->id = 0;
}
 Location2D::Location2D(float x_, float y_, int id_) {
	this->x = x_;
	this->y = y_;
	this->id = id_;
}
float Location2D::getX() {
	return this->x;
}
float Location2D::getY() {
	return this->y;
}
int Location2D::getID() {
	return this->id;
}

void Location2D::setID(int id_) {
	this->id = id_;
}

void Location2D::setX(float x_) {
	this->x = x_;
	
}

void Location2D::setY(float y_) {
	this->y = y_;
}
