#ifndef _PID_h
#define _PID_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdint.h>

class PID
{
public:

	PID(float P, float D, float I);					// This is the class constructor. It is called whenever we create an instance of the PID class 
	//PID();										// Deafault Constructor
	void setGains(float P, float D, float I);		// This function updates the values of the gains
	void reset();									// This function resets any stored values used by the integral or derative terms
	float update(float demand, float measurement);	// This function calculates the PID control signal. It should be called in a loop
	void print_components();						// This function prints the individual components of the control signal and can be used for debugging
	void setMax(float  newMax);						// This function sets the maximum output the controller can ask for
	void setDebug(bool state);						// This function sets the debug flag;
	bool getPError(double val);						// Returns whether the proportional error is smaller/higher than the value
	void printResponse();

private:

	//Control gains
	float Kp; //Proportional
	float Ki; //Integral
	float Kd; //Derivative

	//We can use this to limit the output to a certain value
	float max_output = 255;

	//Output components
	float Kp_output = 0;
	float Ki_output = 0;
	float Kd_output = 0;
	float total = 0;

	//Values to store
	float last_error;			//For calculating the derivative term
	float integral_error;		//For storing the integral of the error
	long last_millis;
	bool debug;
	
	//This flag controls whether we print the contributions of each component when update is called
};

#endif
