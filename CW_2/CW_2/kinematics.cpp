#include "kinematics.h"

Kinematics::Kinematics() {

	x_old = 90;				// Center of the map (in cm) (90,90)
	y_old = 90;
	x_new = 90; 
	y_new = 90;
	d = 0;
	theta_new = 0;
	theta_old = 0;
	last_enc_left = 0;
	last_enc_right = 0;
}

float Kinematics::update(long enc_dist_left, long enc_dist_right) {

	// Update Translation
	d = Kinematics::translate_enc_to_cm(((enc_dist_left + enc_dist_right)/2) - ((last_enc_left + last_enc_right)/2));
	
	x_new = x_old + d * cos(theta_new);
	y_new = y_old + d * sin(theta_new);
		
	x_old = x_new;
	y_old = y_new;

	// Update Rotation
	double d_left = Kinematics::translate_enc_to_cm(enc_dist_left - last_enc_left);
	double d_right = Kinematics::translate_enc_to_cm(enc_dist_right - last_enc_right);

	//theta_new = theta_old + ((d_left - d_right) / WHEEL_DISTANCE);

	theta_new = fmod((theta_old + ((d_left - d_right) / WHEEL_DISTANCE)), (2 * PI));
	theta_old = theta_new;

	last_enc_left = enc_dist_left;
	last_enc_right = enc_dist_right;

	return (theta_new);
}

float Kinematics::update_ICC(long enc_dist_left, long enc_dist_right, double speed_left, double speed_right, double dt) {

	double radius_ICC, ICCx, ICCy, omega;

	//Serial.println(speed_left);
	//Serial.println(speed_right);

	//Serial.println(speed_left - speed_right);
	if (abs(speed_right - speed_left) > 0) {
		radius_ICC = abs((WHEEL_DISTANCE/2)*((speed_left + speed_right) / (speed_right - speed_left)));
		omega = (speed_right - speed_left) / WHEEL_DISTANCE;

		//Serial.println(radius_ICC);
		//Serial.println(omega);
	}
	else {
		//omega = 0.000001;
		//Serial.println("\n Called Normal Update !!! \n");
		// call the normal update
		return update(enc_dist_left, enc_dist_right);
	}

	ICCx = x_old - radius_ICC * sin(-theta_old);
	ICCy = y_old + radius_ICC * cos(-theta_old);

	// Calculate new pose parameters
	x_new = cos(omega*dt)*(x_old - ICCx) - sin(omega*dt)*(y_old - ICCy) + ICCx;
	y_new = sin(omega*dt)*(x_old - ICCx) + cos(omega*dt)*(y_old - ICCy) + ICCy;
	theta_new = fmod((theta_old + omega * dt), (2*PI));

	// prepare for the next iteration
	x_old = x_new;
	y_old = y_new;
	theta_old = theta_new;

	last_enc_left = enc_dist_left;
	last_enc_right = enc_dist_right;

	return (theta_new);
}

float Kinematics::update_ICC_enc_counts(long enc_dist_left, long enc_dist_right, double dt) {

	double radius_ICC, ICCx, ICCy, omega;

	// Calculate speed based on encoder counts.
	
	double speed_left = Kinematics::translate_enc_to_cm(enc_dist_left - last_enc_left) / dt;
	double speed_right = Kinematics::translate_enc_to_cm(enc_dist_right - last_enc_right) / dt;

	//Serial.println(speed_left);
	//Serial.println(speed_right);

	//Serial.println(speed_left - speed_right);
	//if (abs(speed_right - speed_left) > 1) {
	if (abs(speed_right - speed_left) > 0) {
		radius_ICC = abs((WHEEL_DISTANCE/2)*((speed_left + speed_right) / (speed_right - speed_left)));
		omega = (speed_right - speed_left) / WHEEL_DISTANCE;  // is it  possible that he needs radians?
	}
	else {
		//Serial.println("\n Called Normal Update !!! \n");
		// call the normal update
		return update(enc_dist_left, enc_dist_right);
	}

	ICCx = x_old - radius_ICC * sin(theta_old);
	ICCy = y_old + radius_ICC * cos(theta_old);

	// Calculate new pose parameters

	x_new = (cos(omega*dt)*(x_old - ICCx)) - sin(omega*dt)*(y_old - ICCy) + ICCx;
	y_new = (sin(omega*dt)*(x_old - ICCx)) + cos(omega*dt)*(y_old - ICCy) + ICCy;
	
	
	
	theta_new = fmod((theta_old + omega * dt), (2 * PI));

	// prepare for the next iteration
	x_old = x_new;
	y_old = y_new;
	theta_old = theta_new;

	last_enc_left = enc_dist_left;
	last_enc_right = enc_dist_right;

	return (theta_new);
}

/* Helper Functions */

float Kinematics::get_x_cm() {
	return x_new;
}

float Kinematics::get_y_cm() {
	return y_new;
}

float Kinematics::get_x_mm() {
	return x_new*10;
}

float Kinematics::get_y_mm() {
	return y_new*10;
}

float Kinematics::get_theta_radians() {
	return theta_new;
}


double Kinematics::translate_enc_to_cm(long enc_dist) {
	return  ((enc_dist * CM_PER_COUNT ));
}

long Kinematics::translate_cm_to_enc(double enc_dist) {
	return(enc_dist / CM_PER_COUNT);
}

/* long Motor::translate_rot_to_enc(float deg_angle) 
	Returns the distance (in encoder counts) that the motor should move in order to turn the robot
*/
long Kinematics::translate_rot_to_enc(float deg_angle) {

	float angle = deg_angle * (PI / 180);

	// distance each wheel covers when rotating 360 degrees around the center of the robot. 2piR
	float distance = 2 * PI * (WHEEL_DISTANCE / 2);

	// the required encoder counts that need to turn the wheel are : x/0.15, where x the distance covered
	return ((distance*angle / (2 * PI)) / ENC_TO_CM);
}

void Kinematics::print_TM() {
	Serial.print("[Translation (x,y) = (");
	Serial.print(x_new);
	Serial.print(", ");
	Serial.print(y_new);
	Serial.print(") ]");

	Serial.print("[Rotation (theta) = (");
	Serial.print(radiansToDegrees(theta_new));
	Serial.println(") ]");
}