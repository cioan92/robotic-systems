#include "move.h"

int moveType;
long target_left, target_right;
float target_speed_left, target_speed_right;
float target_angle;

float rotation_angle;
double cm_straight;

int move_stages = MOVE_SET_PARAMS;

//last encoder counts for no movement function
long lastRightCountsNoMove, lastLeftCountsNoMove;

/* void perform_move(double trgt, double trgt_speed, int move_type)

	Specifies a movement in terms of how the encoder counts of each wheel will change
	Needed to be called for every action that involves angle/speed/position PID control
*/
void perform_move(double trgt, double trgt_speed, int move_type) {

	switch (move_type) {
	case DIR_FORW: {
		moveType = DIR_FORW;
		target_left = Kinematics::translate_cm_to_enc(trgt) + count_e0;
		target_right = Kinematics::translate_cm_to_enc(trgt) + count_e1;
		target_speed_left = trgt_speed;
		target_speed_right = trgt_speed;
		break;
	}
	case DIR_BCKW: {
		moveType = DIR_BCKW;
		target_left = -Kinematics::translate_cm_to_enc(trgt) + count_e0;
		target_right = -Kinematics::translate_cm_to_enc(trgt) + count_e1;
		target_speed_left = -trgt_speed;
		target_speed_right = -trgt_speed;
		break;
	}
	case DIR_TURN_RIGHT: {
		moveType = DIR_TURN_RIGHT;
		target_angle = trgt;
		target_left = left_motor.getEncRotation(target_angle) + count_e0;
		target_right = -right_motor.getEncRotation(target_angle) + count_e1;
		target_speed_left = trgt_speed;
		target_speed_right = -trgt_speed;
		break;
	}
	case DIR_TURN_LEFT: {
		moveType = DIR_TURN_LEFT;
		target_angle = trgt;
		target_left = -left_motor.getEncRotation(target_angle) + count_e0;
		target_right = right_motor.getEncRotation(target_angle) + count_e1;
		target_speed_left = -trgt_speed;
		target_speed_right = trgt_speed;
		break;
	}
	}
}
/* void turn_to_target(float theta, int direction) {
	Turns to the specified angle (given in degrees)
*/
void turn_to_target(float theta, int direction) {

	Serial.println("Turn to target: ");
	Serial.println(radiansToDegrees(theta));

	// Set the target angle and/or speed
	perform_move(radiansToDegrees(theta), 6.0, direction);
	
	// Reset parameters
	reset_pid(ANGLE_PID);

	// Set the selected PID for the movement
	selected_pid = ANGLE_PID;
}

/* void void move_to_target(double cm_straight) {
	Straight Line movement in a specified distance (in cm)
*/
void move_to_target(double cm_straight) {

	Serial.println("Move to target: ");
	Serial.println(cm_straight);

	perform_move(cm_straight, 8.0, DIR_FORW);

	// Reset parameters
	reset_pid(SPEED_PID);

	// Set the selected PID for the movement
	selected_pid = SPEED_PID;
}

/* void move::move_to_pos(double x, double y)

	Move to a specific position in the x,y cartesian space. Point (0,0)
	is the point where the robot fist started moving
*/
void move_to_pos(double x, double y) {

	//Serial.print("Move to position:: state:");
	//Serial.print(move_stages);

	switch (move_stages){
		case MOVE_SET_PARAMS: {
			// Kinematics - determine the position of point x,y, according to current position of the robot
			double dx = Pose.get_x_cm() - x;
			double dy = Pose.get_y_cm() - y;

			rotation_angle = -(PI) + (atan2(dy, dx) - Pose.get_theta_radians());
			cm_straight = sqrt(dx * dx + dy * dy);

			move_stages = MOVE_1;
			Serial.println("Move Parameters set");
			Pose.print_TM();
			Serial.println("Promted to next step");
			break;
		}
		case MOVE_1: {
			Serial.print("Move [1]:Turn to target:");
			killMotors();

			int selected_dir;
			float selected_rot_angle = radiansToDegrees(abs(rotation_angle));
			if (selected_rot_angle > 180) {
				selected_rot_angle = 360 - selected_rot_angle;
				selected_dir = DIR_TURN_RIGHT;
			}
			else {
				selected_dir = DIR_TURN_LEFT;
			}

			turn_to_target(degreesToRadians(selected_rot_angle), selected_dir);
			Pose.print_TM();
			move_stages = MOVE_1_IN_PROCESS;
			delay(LONG_DELAY);
			break;
		}
		case MOVE_2: {
			Serial.println("Move [2]: Move towards target");
			killMotors();
			move_to_target(cm_straight);
			move_stages = MOVE_2_IN_PROCESS;
			delay(LONG_DELAY);
			break;
		}
		default:
			break;
		}
}

void turn_to_angle(float rotation_angle, int direction) {

	switch (move_stages) {
	case MOVE_SET_PARAMS: {
		move_stages = MOVE_1;
		Serial.println("Promted to next step");
		break;
	}
	case MOVE_1: {
		Serial.print("Move [1]:Turn to target:");
		killMotors();
		turn_to_target(degreesToRadians(abs(rotation_angle)), direction);
		Pose.print_TM();
		move_stages = MOVE_1_IN_PROCESS;
		delay(LONG_DELAY);
		break;
	}
	case MOVE_2: {
		
		// Stop the motors 
		killMotors();

		// Designate move ending
		move_stages = MOVE_ENDED;

		delay(LONG_DELAY);
		break;
	}
	default:
		break;
	}
}

void move_straight(double cm_straight) {

	switch (move_stages) {
	case MOVE_SET_PARAMS: {
		move_stages = MOVE_1;
		Serial.println("Promted to next step");
		break;
	}
	case MOVE_1: {
		Serial.print("Move [1]: Move Straight:");

		killMotors();
		move_to_target(cm_straight);
		move_stages = MOVE_2_IN_PROCESS;
		delay(LONG_DELAY);
		break;
	}
	case MOVE_2: {

		// Stop the motors 
		killMotors();

		// Designate move ending
		move_stages = MOVE_ENDED;

		delay(LONG_DELAY);
		break;
	}
	default:
		break;
	}
}


void killMotors() {
	reset_pid(selected_pid);
	selected_pid = NO_PID;
	left_motor.setSpeed(0);
	right_motor.setSpeed(0);

	target_speed_left = 0;
	target_speed_right = 0;
}

/* int move_status()
	Checks whether the current movement has been completed 
	Returns the state to the outer loop
*/
int move_status() {
	switch (move_stages) {
		case MOVE_1_IN_PROCESS: {
			// Update PID Controller 
			// choose_PID_controller(ANGLE_PID, false);
			// Serial.println(leftAngle.getPError(POSITION_ERROR));
			// Serial.println(rightAngle.getPError(POSITION_ERROR));

			if (leftAngle.getPError(50) && rightAngle.getPError(50)) {
				
				if (getEnc0Speed() + getEnc1Speed() == 0) {

					// Stop refreshing the PID controller
					killMotors();

					// Designate move ending
					move_stages = MOVE_2;
					Serial.println("Move 1 completed");
					break;
				}
			}
			break;
		}
		case MOVE_2_IN_PROCESS: {
			//Serial.println("Move 2 in process");
			// Update Position PID
			choose_PID_controller(POSITION_PID, false);
			if ((leftPose.getPError(POSITION_ERROR) && rightPose.getPError(POSITION_ERROR))) {
				
				// Stop refreshing the PID controller
				killMotors();

				// Designate move ending
				move_stages = MOVE_ENDED;
				Serial.println("Move 2 completed");
				break;
			}
			break;
		}
		default:
			break;
	}
	return move_stages;
}

void reset_move_states() {
	move_stages = MOVE_SET_PARAMS;
}



bool checkNoMovement() {

	long tempTime = 0;
	//|| ( getSpeedLeft() == 0 && getSpeedRight() == 0 )
	if (lastRightCountsNoMove == count_e1 && lastLeftCountsNoMove == count_e0) {
		tempTime = millis() - noMovement;

	}
	else {
		noMovement = millis();
	}
	//Serial.print("Temp: ");
   // Serial.println(tempTime);
   // Serial.print("noMovement: ");
	//Serial.println(noMovement);
	///Serial.print("Now:: ");
	//Serial.println(millis());
	if (tempTime > 3000) {
		return true;
	}
	else {

		return false;
	}

}


//returns true if mvoe finishes
//else false
bool checkIfMoveFinished() {
	//update flag...movement is not complete

	//move_complete = false;
	left_motor.setMaxSpeed(30);
	right_motor.setMaxSpeed(30);

	float left = (abs(target_left - count_e0));
	float right = (abs(target_right - count_e0));


	if (checkNoMovement()) {
		left = 0;
		right = 0;
		Serial.println("No movement");
	}
	lastRightCountsNoMove = count_e1;
	lastLeftCountsNoMove = count_e0;
	
	//TODO add debug function here
	//debugPid(target_left, target_right, output_l, output_r);
	
	if ((left < 10) || (right < 10)) {
		Serial.println("kill motors");
		//Kill motors and return true that move finished
		selected_pid = NO_PID;
		killMotors();
		return true;
	}
	else {
		return false;
	}
}


void createNavList() {

	navPoints[0] =  Location2D(150, 150, 0);

	//navPoints[1] = Location2D(90, 90, 1);

	//navPoints[2] = Location2D(140, 140, 2);

	navPoints[1] =  Location2D(60, 150, 1);

	navPoints[2] =  Location2D(90, 90, 2);

	//navPoints[3] =  Location2D(120, 60, 3);

	//navPoints[4] = Location2D(90, 90, 4);

}

Location2D* getNavList() {
	return navPoints;

}

Location2D getNavPoint(int index) {
	return navPoints[index];
}