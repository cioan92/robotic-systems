// Sets up the Left and Right motors, functions of operation (move, turn etc. )
// 
#include "motors.h"
#include <stdint.h>

const int default_max_speed = 255;

Motor::Motor(byte pwm, byte dir){
	//store pin numbers
	pwm_pin = pwm;
	dir_pin = dir;

	//set pins as outputs
	pinMode(pwm_pin, OUTPUT);
	pinMode(dir_pin, OUTPUT);

	//set initial speed and direction
	digitalWrite(pwm_pin, LOW);
	digitalWrite(dir_pin, LOW);
}

void Motor::setPins(int pwm, int dir) {

	pwm_pin = pwm;
	dir_pin = dir;

	pinMode(pwm_pin, OUTPUT);
	pinMode(dir_pin, OUTPUT);

	digitalWrite(pwm_pin, 0);
	digitalWrite(dir_pin, 0);
}

void Motor::setSpeed(int demand) {
	int mag;

	//Handle setting directions
	if (demand < 0) {
		mag = -1 * demand;
		digitalWrite(dir_pin, 1);
		direction = 1;
	}
	else {
		digitalWrite(dir_pin, 0);
		direction = 0;
		mag = demand;
	}

	if (mag > maxSpeed) {
		mag = maxSpeed;
	}

	//Write to the H-Bridge
	analogWrite(pwm_pin, mag);

	//Store current speed
	if (demand < 0) {
		speed = mag * -1;
	}
	else {
		speed = mag;
	}

	if (debug) {
		Serial.print("PWM Value ");
		Serial.println(mag);
	}
}

void Motor::stop() {
	setSpeed(0);
}

int Motor::setDirection(int dir) {
	//direction = dir;
	//digitalWrite(dir_pin, dir);
}

int Motor::getSpeed() {
	//if (direction == 1){
	//	return (-1)*speed;
	//}
	//else {
	return speed;
	//}	
}

void Motor::setMaxSpeed(int newMax) {
	if (newMax > 255) {
		newMax = 255;
	}
	maxSpeed = newMax;

	if (debug) {
		Serial.print("Max speed: ");
		Serial.println(maxSpeed);
	}
}

/* int Motor::getEncRotation(int angle)
	Returns the distance (in encoder counts) that the motor should move in order to turn the robot
*/
int Motor::getEncRotation(int deg_angle) {

	float angle = deg_angle * (PI / 180);

	// distance each wheel covers when rotating 360 degrees around the center of the robot. 2piR
	float distance = 2 * PI * (WHEEL_DISTANCE / 2);

	// the required encoder counts that need to turn the wheel are : x/0.15, where x the distance covered
	return ((distance*angle / (2 * PI)) / ENC_TO_CM);
}

/* int Motor::getEncDistance(int angle)
	Returns the distance (in encoder counts) that the motor should move in order to move the robot
	at the specified distance (in cm)
*/
int Motor::getEncDistance(float distance) {
	return (distance / ENC_TO_CM);
}

void Motor::setDebug(bool state) {
	debug = state;
}

void Motor::setPower(float demand){
	// Toggle direction based on sign of demand.
	digitalWrite(dir_pin, demand < 0 ? HIGH : LOW);

	// Write out absolute magnitude to pwm pin
	demand = abs(demand);
	demand = constrain(demand, 0, maxSpeed);
	analogWrite(pwm_pin, demand);
}