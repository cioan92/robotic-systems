// kinematics.h

#ifndef _KINEMATICS_h
#define _KINEMATICS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "definitions.h"

const int GEAR_RATIO = 120;
const int COUNTS_PER_SHAFT_REVOLUTION = 2*PI*(WHEEL_DISTANCE/2)/ ENC_TO_CM;
const int COUNTS_PER_WHEEL_REVOLUTION = 2*PI*WHEEL_DIAMETER/ ENC_TO_CM;

class Kinematics{
public:
	static double translate_enc_to_cm(long enc_dist);				// Translate encoder counts to distance
	static double translate_enc_to_rotation(long enc_count);		// Translate encoder counts to rotation
	static long Kinematics::translate_cm_to_enc(double enc_dist);	// Translate distance to encoder counts
	static long Kinematics::translate_rot_to_enc(float deg_angle);	// Translate rotation to encoder counts

	Kinematics();													// Constructor

	float update(long enc_dist_left, long enc_dist_right);			// Update translation (Returns rotation angle in radians)
	float update_ICC(long enc_dist_left, long enc_dist_right, double speed_left, double speed_right, double dt);
	float update_ICC_enc_counts(long enc_dist_left, long enc_dist_right, double dt);
	float Kinematics::get_x_cm();										// Return translation x  (relative to base (0,0))
	float Kinematics::get_y_cm();										// Return translation y  (relative to base (0,0))
	float Kinematics::get_x_mm();										// Return translation x  (relative to base (0,0))
	float Kinematics::get_y_mm();										// Return translation y  (relative to base (0,0))
	float Kinematics::get_theta_radians();							// Return rotation theta (relative to base (0,0))
	void Kinematics::print_TM();									// debug information (translation, rotation)

private:
	double x_old, y_old, x_new, y_new;
	double d;
	float theta_new,theta_old;										// angles (in radians)
	long last_enc_left, last_enc_right;
};

#endif