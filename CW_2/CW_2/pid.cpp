// Proportional Integral Derivative Controller for Romi
#include "pid.h"

/*Here, the definition of the PID class begins. This is indicated by the keyword: "class"
This is a general description of the data and functions that the class contains.
To use a class, we must make a specific instance of the class by declaring it into the same way we declare a variable.
For example, to create a version of the PID class, in our main file we might write:

PID LeftWheelPID;
PID RightWheelPID;

This will create two instances of the PID class; one for the left wheel and one for the right wheel.
Each class will have a full copy of all the variables and functions defined for that particular class.
*/

/*
 * Class constructors
 * This runs whenever we create an instance of the class
 */
/*PID::PID() {

	max_output = 255;
	Kp_output = 0;
	Ki_output = 0;
	Kd_output = 0;
	total = 0;
	last_error = 0;
	integral_error = 0;
	last_millis = 0;
	debug = false;
}*/

PID::PID(float P, float D, float I){
	//Store the gains
	setGains(P, D, I);
	//Set last_millis
	last_millis = 0;

	max_output = 255;
	Kp_output = 0;
	Ki_output = 0;
	Kd_output = 0;
	total = 0;
	last_error = 0; 
	integral_error = 0;
	last_millis = 0;
	debug = false; 
}

/*
 * This function prints the individual contributions to the total contol signal
 * You can call this yourself for debugging purposes, or set the debug flag to true to have it called
 * whenever the update function is called.
 */
void PID::print_components(){
	Serial.print("Proportional : ");
	Serial.print(Kp_output);
	Serial.print(" Differential : ");
	Serial.print(Kd_output);
	Serial.print(" Integral : ");
	Serial.print(Ki_output);
	Serial.print(" Total: ");
	Serial.println(total);
}

/*
 * This function sets the gains of the PID controller
 */
void PID::setGains(float P, float D, float I){
	Kp = P;
	Kd = D;
	Ki = I;
}

/*
 * This is the update function.
 * This function should be called repeatedly.
 * It takes a measurement of a particular quantity and a desired value for that quantity as input
 * It returns an output; this can be sent directly to the motors,
 * or perhaps combined with other control outputs
 */
float PID::update(float demand, float measurement){
	//Calculate how much time (in milliseconds) has passed since the last update call
	long time_now = millis();
	float time_delta = time_now - last_millis;
	last_millis = time_now;

	/*
	 * ================================
	 * Your code goes implementation of a PID controller should go here
	 * ================================
	 */

	 //P. (Pro)This represents the error term
	float error = (demand - measurement);

	//This represents the error derivative
	float error_delta = ((error - last_error) / time_delta);
	last_error = error;

	//Update storage
	integral_error += error*time_delta;

	/*
	 * ===========================
	 * Code below this point should not need to be changed
	 */

	 //Calculate components
	Kp_output = Kp * error;
	Kd_output = Kd * error_delta;
	Ki_output = Ki * integral_error;

	//Add the three components to get the total output
	total = Kp_output + Kd_output + Ki_output;

	//Make sure we don't exceed the maximum output
	if (total > max_output){
		total = max_output;
	}

	if (total < -max_output){
		total = -max_output;
	}

	//Print debugging information if required
	if (debug){
		Serial.print("Error: ");
		Serial.print(error);
		Serial.print(" Error Delta");
		Serial.println(error_delta);
		print_components();
	}
	return total;
}

void PID::setMax(float newMax){
	if (newMax > 0){
		max_output = newMax;
	}
	else{
		Serial.println("Max output must be positive");
	}
}

void PID::setDebug(bool state){
	debug = state;
}

bool PID::getPError(double val) {

	return (last_error < val);
}

void PID::reset(){
	last_error = 0;
	integral_error = 0;
	last_millis = millis();
}

//This function prints measurement / demand - Good for visualising the response on the Serial plotter
void PID::printResponse(){
	
	//float response = last_measurement / last_demand;
	//Serial.println(response);
}