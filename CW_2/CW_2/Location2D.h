#ifndef LOCATION2D_H
#define LOCATION2D_H
class Location2D {
public:
	Location2D();
	 Location2D(float x_, float y_, int id_);
	int getID();
	void setID(int id_);
	float getX();
	float getY();
	void setX(float x_);
	void setY(float y_);
	
	
private:
	float x;
	float y;
	int id;
};

#endif