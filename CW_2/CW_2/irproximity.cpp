#include "irproximity.h"

SharpIR::SharpIR(byte _pin, int threshold_min, int threshold_max){
	pin = _pin;
	distance_threshold_min = threshold_min;
	distance_threshold_max = threshold_max;
}

int SharpIR::getDistanceRaw(){
	return analogRead(pin);
}

/*
 * This piece of code is quite crucial to mapping
 * obstacle distance accurately, so you are encouraged
 * to calibrate your own sensor by following the labsheet.
 * Also remember to make sure your sensor is fixed to your
 * Romi firmly and has a clear line of sight!
 */
 /*
 float SharpIR::getDistanceInMM()
 {

	 float distance = (float)analogRead( pin );

	 // map this to 0 : 5v range.
	 distance *= 0.0048;

	 const float exponent = (1/-0.616);
	 distance = pow( ( distance / 12.494 ), exponent);

	 return distance;
 }*/

float SharpIR::get_distance_in_mm() {

	IR_raw = analogRead(pin);

	// Calculate the distance in mm 
	switch (pin) {
		case A0: { IR_calibr = 10 * IR_calibration_IRA0(IR_raw); return IR_calibr; }
		case A1: { IR_calibr = 10 * IR_calibration_IRA1(IR_raw); return IR_calibr; }
		case A11:{ IR_calibr = 10 * IR_calibration_IRA11(IR_raw); return IR_calibr; }
	}
}

float SharpIR::get_distance_in_cm() {

	IR_raw = analogRead(pin);

	// Calculate the distance in cm 
	switch (pin) {
	case A0: { IR_calibr =  IR_calibration_IRA0(IR_raw); return IR_calibr; }
	case A1: { IR_calibr =  IR_calibration_IRA1(IR_raw); return IR_calibr; }
	case A11: { IR_calibr =  IR_calibration_IRA11(IR_raw); return IR_calibr; }

	}
}

void SharpIR::print_IR_measurement() {

	Serial.print("Measurement (raw): ");
	Serial.print(IR_raw);
	Serial.print(" (cm): ");
	Serial.println(get_distance_in_cm());
}

bool SharpIR::object_within_range(int min, int max) {

	float distance = get_distance_in_cm();

	if ((get_distance_in_cm() < max) && (get_distance_in_cm() > min)) {
		Serial.print("true");
		return true;
	}
	else {
		return false;
	}
}

bool SharpIR::object_detected(int t_type) {
	
	int distance_threshold;
	switch (t_type) {
		case TMAX: {distance_threshold = distance_threshold_max ; break; }
		case TMIN: {distance_threshold = distance_threshold_min ; break; }
	}
	
	if (get_distance_in_cm() < distance_threshold) {
		return true;
	}
	else {
		return false;
	}
}

void SharpIR::set_IR_threshold(int value, int  t_type) {
	switch (t_type){
		case TMAX: {distance_threshold_max = value; break; }
		case TMIN: {distance_threshold_min = value; break; }
	}
}

int SharpIR::get_IR_threshold(int  t_type) {
	
	switch (t_type) {
	case TMAX: {return distance_threshold_max; }
	case TMIN: {return distance_threshold_min; }
	}
	
}
